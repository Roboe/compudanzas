# par y danza

estrategia básica para detectar posibles errores de transmisión / recepción de una secuencia binaria, como parte de {las danzas}.

implementamos el cómputo y detección de la llamada paridad: dada una secuencia de dos tipos de símbolos, ¿la cantidad de símbolos de uno de esos tipos, es par o impar?


# componentes

usaremos el concepto de estados y tiempos discretos.

## participantes

necesitamos al menos dos personas:

* transmisora de secuencia
* calculadora de paridad

## movimientos

necesitamos un alfabeto de dos movimientos:

* un movimiento "alto"
* un movimiento "bajo"

y un conteo de tiempos discretos.

cada uno de esos tiempos que contiene a un movimiento, lo podemos considerar un "bit": unidad mínima de información.

para transmitir información más compleja, necesitamos varios "bits": una secuencia de tiempos con uno de estos dos movimientos.


## estados y transiciones

en cada unidad de tiempo, estamos ya sea en el estado que corresponde al movimiento alto, o en el que corresponde al movimiento bajo.

inicialmente estamos en el movimiento bajo.

tenemos dos tipos de transiciones:

* continúa: sigue en el mismo estado
* invierte: cambia al otro estado


# guía

## cálculo de paridad

de acuerdo al mensaje binario a transmitir, en cada tiempo la persona transmisora envía uno de los dos movimientos posibles.

cuando la cantidad de movimientos que envía en una transmisión son 8, estará enviando un "byte".

la persona calculadora inicialmente está en el movimiento bajo.

en cada tiempo de transmisión, la persona calculadora ha de actuar de la siguiente forma:

* si el movimiento transmitido es bajo, ella ha de continuar su movimiento.
* si el movimiento transmitido es alto, ella ha de invertir su movimiento.

al concluir la transmisión, la persona calculadora habrá terminado:

* en el movimiento alto si la cantidad de movimientos altos transmitidos fue impar. 
* en el movimiento bajo si la cantidad de movimientos altos transmitidos fue par

comprobemos que sí sea así, con secuencias de diferente longitud y paridad.


## detección de error con paridad

### conceptos

el estado en el que terminó la persona calculadora puede llamarse el bit de paridad.

un aspecto interesante y no obvio de este sistema, es que si este bit de paridad se agrega a la secuencia original para formar una nueva secuencia completa, entonces la cantidad total de movimientos altos siempre será un número par:

* cuando la cantidad era impar (por ejemplo, 3), el bit de paridad es alto, y al sumarse da un total par (por ejemplo, 4)
* cuando la cantidad era par (por ejemplo, 2), el bit de paridad es bajo y no se suma al total, que queda par (por ejemplo, 2)

### actividad

después de calcular localmente la paridad de la secuencia a transmitir, podemos pasar a transmitir a mayor distancia la nueva secuencia con el bit de paridad al final:

en cada tiempo, la persona transmisora envía el movimiento correspondiente para transmitir la secuencia completa.

la persona calculadora inicialmente está en el movimiento bajo.

en cada tiempo de transmisión/recepción, la persona calculadora ha de actuar de la siguiente forma:

* si el movimiento recibido es bajo, ella ha de continuar su movimiento.
* si el movimiento recibido es alto, ella ha de invertir su movimiento.

al terminar la transmisión de la secuencia completa, revisamos el estado de la persona calculadora:

* si se encuentra en movimiento alto (paridad impar), hubo algún error en la comunicación
* si se encuentra en movimiento bajo (paridad par), probablemente no hubo error en la comunicación

probemos con secuencias de diferente longitud, a diferentes velocidades, y también a diferentes distancias.

¿llega a suceder que detectamos un error? 

¿qué pasa cuando hay dos errores en la comunicación? ¿es posible que el sistema no lo detecte?
