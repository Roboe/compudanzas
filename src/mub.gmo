# máquina universal bailable

una de {las danzas} en donde bailamos con y a partir de una fila / tira / cinta de objetos y una tabla específica de estados, en un acomodo tal que nos permite simularnos bailando con y a partir de una fila / tira / cinta de objetos y cualquier tabla específica de estados.

implementación especial de una máquina de turing {d-turing}, con la capacidad de simular, emular, ser, cualquier otra máquina de turing.

# contexto 

> no one has seriously proposed the use of a turing machine structure for any practical computation. indeed, i do not even know of one built for demonstration purposes (minsky, 1967, p128)

> we will see that these machines are horribly inefficient and slow - so much so that no one would ever waste their time building one except for amusement - but that, if we are patient with them, they can do wonderful things (feynman, 1996, p54)

# la máquina

esta máquina está basada en la máquina universal de turing desarrollada y explicada en estos libros:

* computation: finite and infinite machines - marvin minsky 1967
* feynman lectures on computation - richard feynman 1996

## símbolos en la cinta

la máquina trabaja con los siguientes seis símbolos en su cinta.

=> ./img/dibujo_mub_simbolos-cinta.png los seis símbolos a utilizar en la cinta

los símbolos ▯, ◫, corresponden a codificar "0" y "1" respectivamente.

## estado inicial de la cinta

la cinta de la máquina tiene la propiedad de contener la descripción de una máquina de turing a simular / ejecutar, en forma de quintuplas binarias; y además contiene la cinta de esa máquina de turing en cuestión.

la cinta tiene un extremo que llamamos "origen" o "centro" (espiral en la imagen), y la cabeza ha de empezar en la posición indicada.

=> ./img/dibujo_mub_cinta-inicial.png dibujo de una persona al lado de una combinación extensa de varios símbolos en la cinta

de donde está la cabeza, hacia el origen, lo que se encuentra en la cinta es un conjunto de quintuplas binarias correspondientes a la máquina de turing a simular, separadas por * y terminadas con un triángulo. este ejemplo utiliza la máquina "contador alternado" documentada en {máquinas de turing}

de donde está la cabeza, hacia el otro lado, lo primero que se encuentra es una codificación binaria del estado y símbolos en cinta actuales de la máquina simulada. en esa misma dirección, después del triángulo, se encuentra la cinta de la máquina simulada, con un * indicando la posición de su respectiva cabeza.

## estados de la máquina

la máquina cuenta con 23 estados posibles, expresados con las siguientes figuras:

=> ./img/ilustracion_symbols_mub.png Symbols corresponding to the 23 choreographic configurations in the rite of computing. They contain the formulas that describe the operations to perform during each of them.

=> https://ipfs.io/ipfs/QmVFYxZv2vjhz3pCkCfEvgGktL2dNbV55KfRPFRqyUJJVX/ilustracion_simbolos_mub.png full image of the symbols (png, ~1.5MB)

las figuras con bordes curvos indican mover la cabeza hacia el origen, y las que tienen bordes rectos indican mover la cabeza lejos del origen.

las figuras solo indican los símbolos en la cinta (en naranja) que causan algun efecto: 

* en verde está el símbolo al cual hay que transformar el símbolo leído
* en amarillo está el estado siguiente al que hay que transicionar.

## ciclo de la máquina

hay que repetir la secuencia indicada a continuación:

* mueve la cabeza en la dirección indicada por la forma de la figura de estado.
* observa cuál es el nuevo símbolo activo en la cinta, y búscalo en la figura del estado.
* si hay un nuevo símbolo en verde, transforma el símbolo activo a este.
* si hay un nuevo estado indicado en amarillo, busca la figura correspondiente, y transiciona a ese estado .

# en escena

una versión de la máquina universal bailable se presentó como acto escénico, uno de nuestros {performances}

=> ./la_consagración_de_la_computadora.gmi {la consagración de la computadora}

además, se ha escrito al respecto como parte de nuestras {proposals}:

=> ./dancing_a_universal_turing_machine.gmi {dancing a universal turing machine}

# archivo

en esta página de {chsnec} se habla (en inglés) de su desarrollo e iteraciones:

=> https://ipfs.io/ipfs/QmYYqFXowEuak9NVNMHexrwZQkEnfH4J6weSPpv3kjFP94/mub/process.html m.u.b. proceso
=> https://ipfs.io/ipfs/QmYYqFXowEuak9NVNMHexrwZQkEnfH4J6weSPpv3kjFP94/mub/instructions.html m.u.b. basic score
