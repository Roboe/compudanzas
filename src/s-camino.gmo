# s-camino... el camino del ensamblador (?)

veo razones varias, múltiples, para el estudio poético y contemplativo de la computación a bajo nivel.

específicamente aquí me refiero al programar en el llamado lenguaje ensamblador (¿o idioma ensamblador?)

en otros contextos se puede hablar de que conviene hacerlo buscando eficiencia, velocidad, haciendo el sacrificio de descender por las escaleras para mover los fierros; ensuciarse las manos mientras se observa la complejidad del dispositivo... pero todo sea por la productividad y el resultado óptimo que se encontrará a final del tunel.

aquí no estamos en ese contexto, claramente. pero justo esa impracticidad, esa mugre, esa manualidad o artesanalidad, la imagen de que a fin de cuentas (solo) estamos moviendo bits de aquí para allá (conectados de tal manera que algo más sucede) es la que llama la atención.

¿qué tal si acompañamos a la máquina en un nivel cercano al que será su propósito (¿o condena?) de vida?

tomamos conciencia de cada uno de los pasos por la que la haremos pasar sin otra opción más que seguir, seguir, seguir…

y sí, al escribir y programar en ensamblador vamos con lentitud, porque las convenciones son extrañas (todo un dialecto que tal vez busca alejar a quien no tiene cierto perfil), porque es un mundo que contrasta con la costumbre de la eficiencia, de los resultados inmediatos.

además, ¿cuál es la prisa, por qué querer hacer tanto?

hacer un programa se puede convertir en un ritual; la invocación del libro de instrucciones y conexiones, la planeación y reflexión antes de escribir la primera letra; tardarse horas en algo que aquellas estructuras de facilidad, practicidad, te permiten hacer en minutos, segundos (¿pero te quitan toda la magia, todo el asombro y confrontación con lo que realmente está pasando?)

la no portabilidad del código está a nuestro favor, porque implica rehacerlo cada vez (o cuando sea necesario), re-contemplar, ir lento. de nuevo, ¿cuál es la prisa?

estoy de acuerdo, es un mundo inaccesible (¿similar a cualquier otro tipo de hechicería?), pero parte de la labor que me autoasigno es hacerle puertas (o grietas, hoyos, y adentro madrigueras) para poderlo compartir.

no cualquiera tiene el tiempo... igual aquí andaremos, con paciencia, y un paso a la vez.

(escrito el 12019-07-05)
