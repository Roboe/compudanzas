# tutorial de uxn

una guía introductoria y pausada para programar la computadora varvara basada en el núcleo {uxn}.

> El ecosistema de Uxn es un espacio de juego y exploración de computación personal, creado para desarrollar y utilizar pequeñas herramientas y juegos y programable en su propio lenguaje ensamblador.

=> https://100r.co/site/uxn.html 100R - uxn

también puedes conseguir una versión "offline" de este material y apoyar nuestros proyectos, con el libro {introducción a programación uxn}.

esta es una traducción del {uxn tutorial} a cargo de ~jota.

=> https://texto-plano.xyz/~jota/ /casa de ~jota (web)
=> gemini://texto-plano.xyz/jota/ /casa de ~jota (gemini)

# día 1

en esta primera sección del tutorial vamos a hablar de los aspectos básicos de la computadora uxn llamada varvara, su paradigma de programación, su arquitectura y por qué podrías querer aprender a programar en ella.

también vamos a saltar directo a nuestros primeros programas simples para demostrar conceptos fundamentales que desarrollaremos en los días siguientes.

{tutorial de uxn día 1}

# día 2

en esta sección vamos a empezar a explorar los aspectos visuales de la computadora varvara: ¡hablamos sobre los aspectos fundamentales del dispositivo de pantalla para que podamos empezar a dibujar en ella!

también discutiremos el trabajo con cortos (2 bytes) además de los números de un solo byte en uxntal.

{tutorial de uxn día 2}

# día 3

aquí introducimos el uso del dispositivo controlador en la computadora varvara: esto nos permite agregar interactividad a nuestros programas y empezar a implementar control de flujo en uxntal.

también hablamos de instrucciones lógicas y de manipulación de la pila en uxntal.

{tutorial de uxn día 3}

# día 4

aquí hablamos del bucle de animación del ordenador varvara, a través de su vector de dispositivo de pantalla.

también hablamos del uso de la memoria del programa como un espacio para datos usando "variables".

{tutorial de uxn día 4}

# día 5

aquí introducimos el dispositivo de ratón varvara para explorar más posibles interacciones y cubrimos los elementos restantes de uxntal y uxn: la pila de retorno, el modo de retorno y el modo de retención.

también discutimos posibles estructuras para crear bucles y programas más complejos utilizando estos recursos.

{tutorial de uxn día 5}

# día 6

aquí hablamos de cómo podemos integrar todo lo que hemos cubierto para crear subrutinas y programas más complejos para el ordenador varvara.

basamos nuestra discusión en una recreación del clásico juego pong. 

además de utilizar estrategias y fragmentos de código anteriores, cubrimos estrategias para dibujar y controlar sprites de varios tiles y para comprobar las colisiones.

{tutorial de uxn día 6}

# día 7

aquí hablamos de los dispositivos del ordenador varvara que aún no hemos cubierto: audio, archivo y fechahora o "datetime".

este debería ser un final ligero y tranquilo de nuestro recorrido, ya que tiene que ver menos con la lógica de programación y más con las convenciones de entrada y salida en estos dispositivos.

{tutorial de uxn día 7}

# apéndices

## apéndice a: repetir un tile dentro de un rectángulo

aquí generalizaremos un procedimiento similar en una subrutina dibuja-tiles que dibuje un rectángulo relleno con un tile dado.

{tutorial de uxn apéndice a}

# índice tentativo

este índice está aquí y ahora como referencia de la estructura general del tutorial.

## día 1: aspectos básicos

* ¿por qué uxn?
* notación {postfix}
* arquitectura de la computadora uxn
* instalación y toolchain
* un hola mundo muy básico
* etiquetas, macros y runas
* un hola mundo mejorado
* imprimir un dígito

instrucciones nuevas: LIT, DEO, ADD, SUB

=> https://git.sr.ht/~rabbits/uxn/ uxn repo

## día 2: la pantalla

* modo corto
* colores del sistema
* dibujar pixeles
* sprites: formato chr, nasu
* dibujar sprites sprites
* operaciones en la pila
* practica: reoetición manual de un sprite

instructiones nuevas: DEI, MUL, DIV, SWP, OVR, ROT, DUP, POP

modo nuevo: modo corto

=> https://wiki.xxiivv.com/site/nasu.html nasu

## día 3: interactividad con el teclado

* vector de controlador
* control de flujo: condicionales, saltos relativos y absolutos
* runas para direcciones
* botón y tecla
* máscaras bitwise
* práctica: mover/cambiar sprite con el teclado

instrucciones nuevas: EQU, NEQ, JCN, JMP, AND, ORA, EOR, SFT

## día 4: bucles y animación

* control de flujo: repetición de un sprite
* vector de pantalla
* variables: página cero, relativas, absolutas
* offsets en direcciones
* timing de animación
* práctica: sprite animado

instrucciones nuevas: LTH, GTH, STZ, STR, STA, LDZ, LDR, LDA

## día 5: interactividad con el mouse

* dispositivo y vector del mouse
* Pila de retorno y modo
* subrutinas: parametros, llamada y retorno
* práctica: sprite como puntero
* práctica: herramienta de dibujo simple

instrucciones nuevas: STH, JSR 

modo nuevo: modo de retorno

## día 6: audio

* el dispositivo de audio
* samples como sprites de audio
* adsr
* pitch
* práctica: pequeño instrumento musical

## día 7: modo de retención y otros dispositivos

* modo de retención
* re-escribiendo código con modo de retención
* dispositivo de archivo: guardando y cargando un estado simple
* dispositivo de fecha y hora: leyendo fecha y hora
* práctica: visualización de fecha y hora

modo nuevo: modo de retención

## día 8: tiempo de demo

* comparte lo que has creado :)

# apoyo

si este tutorial te ha sido de ayuda, considera compartirlo y brindarle tu {apoyo} :)
